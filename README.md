A script to move logs a week and older from SSD storage to bulk HDD storage.

Right now this script only runs on Ubuntu 16.04, but I will edit the [Dockerfile](https://gitlab.com/oct8l/move-logs/blob/Dockerfile/Dockerfile) and create new images as it is uses on different OSes.