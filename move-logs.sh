#!/usr/bin/env bash

# Compress logs
DATE=`date '+%Y-%m-%d'`
touch ./log_to_backup.txt
cat /dev/null > ./log_to_backup.txt
find /var/log/syslog-ng -maxdepth 2 -type f -name "*.log*" -mtime +6 | sort -k 1 -n >> ./log_to_backup.txt
tar -zcf Log_File_Backups_$DATE.tgz -T ./log_to_backup.txt
while read file; do 
rm "$file"; done < ./log_to_backup.txt
# Move compressed logs
mkdir /media/syslogArchive/$DATE
mv Log_File_Backups_$DATE.tgz /media/syslogArchive/$DATE
echo 'Moved' $DATE 'to archive.' >> ./backed_up.txt
rm ./log_to_backup.txt